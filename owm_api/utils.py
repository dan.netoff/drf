import requests
import json
import xml.etree.ElementTree as ET
from datetime import datetime
from sparklines import sparklines


owm = {
    "01d": "☀",
    "02d": "🌤",
    "03d": "⛅",
    "04d": "🌥",
    "09d": "🌧",
    "10d": "🌦",
    "11d": "🌨⚡",
    "13d": "❄",
    "50d": "🌫",

    "01n": "☀",
    "02n": "🌤",
    "03n": "⛅",
    "04n": "🌥",
    "09n": "🌧",
    "10n": "🌦",
    "11n": "🌨⚡",
    "13n": "❄",
    "50n": "🌫",
}


def degToCompass(num):
    val = int((num/22.5)+.5)
    arr = [
        "Север",
        "Северо-северо-восток",
        "Северо-восток",
        "Востоко-северо-восток",
        "Восток",
        "Востоко-юго-восток",
        "Юго-восток",
        "Юго-юго-восток",
        "Юг",
        "Юго-юго-запад",
        "Юго-запад",
        "Западо-юго-запад",
        "Запад",
        "Западо-северо-запад",
        "Северо-запад",
        "Северо-северо-запад",
    ]
    return arr[(val % 16)]


def my_spark_week(my_arr):
    my_sprk = []
    for line in sparklines(my_arr, num_lines=2):
        my_sprk.append(line)
    return my_sprk


def get_weather():
    api_key = "f3282ce65f784fcbcdf35d27b4a91f62"

    city_id = "692709"
    api_url = f"https://api.openweathermap.org/data/2.5/forecast?id={city_id}&appid={api_key}&units=metric&lang=ru"
    print(api_url)
    res = requests.post(api_url)
    my = res.json()
    print(json.dumps(my, indent=4, sort_keys=True))
    my_spark = []

    for my_dt in my["list"]:
        my_d = datetime.fromtimestamp(my_dt["dt"])
        my_H = my_d.strftime('%H')

        my_t = my_dt["main"]
        my_weather = my_dt["weather"][0]
        my_wind = my_dt["wind"]
        my_clouds = my_dt["clouds"]

        my_rain = my_dt.get('rain', )
        my_snow = my_dt.get('snow', )
        if "00" in my_H:
            my_spark.append(None)
        if my_H in ["00", "12"]:
            my_spark.append(int(round(float(my_t['temp']), 0)))


        print(
            my_d.strftime('%d-%m %H'),
            f"🌡{round(float(my_t['temp']), 0)}°C,",
            f"👃{round(float(my_t['feels_like']), 1)}°C,",
            f"Давление:{my_t['pressure']}мм,",
            f"Влажность:{my_t['humidity']}%,",
            f"{my_weather['description']}, {owm[my_weather['icon']]},",
            f"тучи:{my_clouds['all']}%,",
            f"Ветер: {my_wind['speed']}м/с, порыв {my_wind['gust']}м/с {degToCompass(float(my_wind['deg']))}",
            my_dt["pop"],
        )
    my_spr = my_spark_week(my_spark)
    for line in my_spr:
        print(line)


# get_weather()

def get_sun_time(date="2022-03-18"):
    lat = "49.9694920053368"
    lon = "35.69869437717412"
    api_url = f"https://api.sunrise-sunset.org/json?lat={lat}8&lng={lon}&formatted=0&date={date}"

    response = '''{"results":{"sunrise":"2022-03-18T03:42:38+00:00","sunset":"2022-03-18T15:47:55+00:00",
    "solar_noon":"2022-03-18T09:45:17+00:00","day_length":43517,"civil_twilight_begin":"2022-03-18T03:12:07+00:00",
    "civil_twilight_end":"2022-03-18T16:18:26+00:00","nautical_twilight_begin":"2022-03-18T02:34:16+00:00",
    "nautical_twilight_end":"2022-03-18T16:56:18+00:00","astronomical_twilight_begin":"2022-03-18T01:55:11+00:00",
    "astronomical_twilight_end":"2022-03-18T17:35:23+00:00"},"status":"OK"}
    '''

# {
#     "id": 692709,
#     "name": "Staryy Merchyk",
#     "state": "",
#     "country": "UA",
#     "coord": {
#         "lon": 35.759041,
#         "lat": 49.981468
#     }
# },
