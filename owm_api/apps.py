from django.apps import AppConfig


class OwmApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'owm_api'
