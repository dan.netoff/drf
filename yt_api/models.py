from django.db import models


class YtChannelGroup(models.Model):
    group_name = models.CharField(max_length=200)

    def __str__(self):
        return self.group_name

    class Meta:
        verbose_name = "Группы"
        verbose_name_plural = "Групировка"


class YtChannel(models.Model):
    cid = models.CharField(max_length=24)
    title = models.CharField(max_length=250)
    description = models.TextField(null=True)
    thumbnails_url = models.CharField(max_length=250, null=True)
    registration_date = models.DateTimeField()

    date_add = models.DateTimeField(auto_now_add=True)
    date_edit = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "<YtChannel> " + str(self.title)

    class Meta:
        verbose_name = "Каналы"
        verbose_name_plural = "Список Каналов"


class YtChannelStats(models.Model):
    subscribers = models.IntegerField(default=0)
    videos = models.IntegerField()
    views = models.IntegerField()

    channel = models.ForeignKey(YtChannel, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Статистика"
        verbose_name_plural = "Статистика"
