from rest_framework import serializers

from .models import YtChannelGroup, YtChannel


class YtChannelGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = YtChannelGroup
        fields = "__all__"


class YtChannelSerializer(serializers.ModelSerializer):
    class Meta:
        model = YtChannel
        fields = "__all__"