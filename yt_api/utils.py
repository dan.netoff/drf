import requests
import validators
import xml.etree.ElementTree as ET
from typing import Dict, Tuple


# NewLine
NL = "\n"

NGROK_HOST = "https://d75e-195-140-224-118.eu.ngrok.io"
CALLBACK_URL = NGROK_HOST + "/callback/"

YT_PUBSUB_HOST = "https://pubsubhubbub.appspot.com/subscribe"
YT_TOPIC_URL = "https://www.youtube.com/xml/feeds/videos.xml?channel_id="
YT_VIDEO_URL = "https://www.youtube.com/watch?v="
YT_API_KEY = "AIzaSyCdtstq91hiGxXylT_p7RGpNxKazcKwY4U"
YT_API_CHANNEL_INFO_URL = "https://youtube.googleapis.com/youtube/v3/channels?"
YT_API_CHANNEL_INFO_URL += "part=snippet%2Cstatistics&id={id}&key={key}"
YT_VIDEO_PREVIEW_URL = "http://i3.ytimg.com/vi/{video_id}/hqdefault.jpg"

TG_TOKEN = "1698878545:AAGMW0rU_zTaPtrWYs3Kx_D5QgtO0P6BWuQ"
TG_MY_CHAT_ID = 472141107
TG_API_URL = f"https://api.telegram.org/bot{TG_TOKEN}/"
TG_SEND_MESSAGE_URL = f"{TG_API_URL}/sendMessage"

# NameSpaces from YT_Xml
XML_NS = {
    "yt": "http://www.youtube.com/xml/schemas/2015",
    "def": "http://www.w3.org/2005/Atom",
}


yt_ch_rust = {
    "UCtQ3NfmlxDDWyhfxxgOsJhA": "Frost",
    "UCcw0KZs8oa3QgHnckw7EXXA": "Facepunch",
    "UCj-wTLj0p0YvTkS-9-ACm0A": "Blooprint",
    "UCr_s8NB3oUbhyJOvaJIXqVw": "Posty",

    "UCnMi24EZ0Lc52yoi5nvMcvA": "сачелька",
    "UCLKHPmYnzMAJV9AVrWRrusA": "esQa",
    "UCJUm_JzaIg21KANvUme6TOw": "MAGICOW",
    "UCAFwox2gQw7YsJymIJUPLQg": "невер лаки",
    "UCd6bVsWYdMay62NS952dPOg": "Norch",
    "UCft_yYBZJU0u1rUkwLTlzlQ": "sHuMa",
    "UCv4Dn_vNfQ2ucQw_dZkC7Gw": "Woodcock",
    "UCocvwThNlcQ8pdRFnXgpGbw": "chistobzden",
    "UCOyKtXwAm6ALJ1-r3nugR5Q": "CheZee",
    "UCbehzEsII2SDFIMrWvIdFmA": "LEGUNDA",
    "UCzobeR1SAyGvHHO0XVvyDCw": "HumenBrain",
    "UCIwcC7QMLfAu0mF9H_9j50g": "KARMAN",
    "UCuX8NmPXBu8iiDfxggCegaw": "EvKa's PLACE",
}

yt_ch_polit = {
    "UCUGfDbfRIx51kJGGHIFo8Rw": "Максим Кац",
    "UCMEiyV8N2J93GdPNltPYM6w": "Espreso.TV",
    "UCCcprrrcbdaj14kYPjcbj9w": "В гостях у Гордона",
    "UCkp0Tc7ll67bChomTyB1ezQ": "Om TV",
    "UCOA1y6Wu0kug8rbOpvHFZEQ": "OmTV UA",
    "UC7Elc-kLydl-NAV4g204pDQ": "Популярная политика",
    "UCsAw3WynQJMm7tMy093y37A": "Алексей Навальный",
    "UCgxTPTFbIbCWfTR9I2-5SeQ": "Навальный LIVE",
    "UCI4hEQc8mkuK2eMsew_MwTw": "Любовь Соболь",
    "UC101o-vQ2iOj9vr00JUlyKw": "varlamov",
    "UC97O_ce8Rm7rE0hjE82joaA": "OBOZREVATEL TV",
    "UCZb1iJo6LoM_oPJ_vM4J4_Q": "Fantomas",
    "UC5HBd4l_kpba5b0O1pK-Bfg": "STERNENKO",
    "UCjWy2g76QZf7QLEwx4cB46g": "Alexey Arestovych",
    "UC2oGvjIJwxn1KeZR3JtE-uQ": "hromadske",
    "UCjAg2-3PgoksLAkYE88S_6g": "Гроші",
    "UCdIp4tcWOGihEQKYxzSlFaQ": "МИР НАИЗНАНКУ",
    "UCkUe2e1YuRtBRRaP2XTupwg": "BIHUS info",
}

yt_ch_test = {
    "UCPF-oYb2-xN5FbCXy0167Gg": "testest",
}

example = '''   
    <?xml version="1.0" encoding="UTF-8"?>
    <feed xmlns:yt="http://www.youtube.com/xml/schemas/2015" xmlns="http://www.w3.org/2005/Atom">
        <link rel="hub" href="https://pubsubhubbub.appspot.com"/>
        <link rel="self" href="https://www.youtube.com/feeds/videos.xml?channel_id=UCJaorbJkgbef6GeeRb-6HnQ"/>
        <title>YouTube video feed</title>
        <updated>2022-03-13T20:26:35.394310927+00:00</updated>
        <entry>
            <id>yt:video:FZ3aAIDGZKI</id>
            <yt:videoId>FZ3aAIDGZKI</yt:videoId>
            <yt:channelId>UCJaorbJkgbef6GeeRb-6HnQ</yt:channelId>
            <title>20210410 174422</title>
            <link rel="alternate" href="https://www.youtube.com/watch?v=FZ3aAIDGZKI"/>
            <author>
                <name>Dan Netoff</name>
                <uri>https://www.youtube.com/channel/UCJaorbJkgbef6GeeRb-6HnQ</uri>
            </author>
            <published>2022-03-13T20:26:02+00:00</published>
            <updated>2022-03-13T20:26:35.394310927+00:00</updated>
        </entry>
    </feed>
'''


def yt_decode_xml_to_msg(my_xml: str) -> Dict[str, str]:
    root = ET.fromstring(my_xml)
    entry = root.find("def:entry", XML_NS)
    msg = {}
    if entry:
        msg["video_id"] = entry.find("yt:videoId", XML_NS).text
        msg["channel_id"] = entry.find("yt:channelId", XML_NS).text
        msg["title"] = entry.find("def:title", XML_NS).text
        msg["author"] = entry.find("def:author", XML_NS).find("def:name", XML_NS).text
        return msg
    return msg


def yt_get_channel_id(link: str) -> str:
    shift1 = 20
    shift2 = 30
    cid_len = 24
    res = None
    cid = ""
    try:
        res = requests.get(link)
    except requests.exceptions.ConnectionError:
        print("** yt_get_channel_id = 404 page not found")
    if res:
        pos1 = res.text.find("browse_id")
        pos2 = res.text.find('itemprop="channelId"')
        if pos1 > 0:
            cid = res.text[pos1+shift1:pos1+shift1+cid_len]
        elif pos2 > 0:
            cid = res.text[pos2+shift2:pos2+shift2+cid_len]

        if '"' not in cid and '}' not in cid:
            return cid
    return ''


def int_space_separator(my_num: int) -> str:
    return '{:,}'.format(int(my_num)).replace(',', ' ')


def int_to_subscribers(my_num: int) -> str:
    print(my_num)
    if my_num > 999999:
        res = ((my_num * 100) // 1_000_000) / 100
        str_res = 'млн.'
    elif my_num > 59999:
        res = my_num // 1_000
        str_res = 'тыс.'
    elif my_num > 9999:
        res = ((my_num * 10) // 1_000) / 10
        str_res = 'тыс.'
    elif my_num > 999:
        res = ((my_num * 100) // 1_000) / 100
        str_res = 'тыс.'
    else:
        res = my_num
        str_res = ""
    return f"{res} {str_res} подписчиков"


def yt_get_channel_info(cid: str) -> Dict[str, str]:
    res = None
    try:
        res = requests.get(YT_API_CHANNEL_INFO_URL.format(id=cid, key=YT_API_KEY))
        print(YT_API_CHANNEL_INFO_URL.format(id=cid, key=YT_API_KEY))
    except requests.exceptions.ConnectionError:
        print("*yt_get_channel_info NO INTERNET")
    if res:
        ch = res.json()["items"][0]
        my_ch = {
            "cid": cid,
            "title": ch["snippet"]["title"],
            "description": ch["snippet"]["description"],
            "publishedAt": ch["snippet"]["publishedAt"],
            "thumbnails": (
                ch["snippet"]["thumbnails"]["default"]["url"]
                .replace("s88-c-k-c0x00ffffff-no-rj", "s260")
            ),
            "viewCount": int_space_separator(ch["statistics"]["viewCount"]),
            "subscriberCount": int_to_subscribers(int(ch["statistics"]["subscriberCount"])),
            # "subscriberCount": int_space_separator(ch["statistics"]["subscriberCount"]),
            "videoCount": int_space_separator(ch["statistics"]["videoCount"]),
        }
        return my_ch
    return {}


def tg_send_message(token: str, chat_id: str, msg: Dict[str, str], options: Tuple[bool, bool]):
    headers = {'charset': 'utf-8'}
    text = msg["author"] + NL
    text += msg["title"] + NL
    text += YT_VIDEO_URL + msg["video_id"]
    my_msg = {
        "chat_id": chat_id,
        "text": text,
        "disable_web_page_preview": options[0],
        "disable_notification": options[1],
    }
    print(text)
    r = requests.post(TG_SEND_MESSAGE_URL, json=my_msg, headers=headers)
    return r.json()


def yt_subscriber(channel_id, hub_mode="subscribe"):
    headers = {'Content-type': 'application/x-www-form-urlencoded', 'Accept': 'text/plain'}
    hub_topic = YT_TOPIC_URL + channel_id
    hub_json = {
        "hub.callback": CALLBACK_URL,
        "hub.topic": hub_topic,
        "hub.verify": "async",
        "hub.mode": hub_mode,
    }
    r = requests.post(YT_PUBSUB_HOST, data=hub_json, headers=headers)
    if r.status_code in (202, 204):
        res = f"{r.status_code}, SUBSCRIBE TO : {channel_id}"
    else:
        # AND NOW pubsub Always return 202 ! Big problem!
        res = f"{r.status_code}, CANT SUBSCRIBE : {channel_id}"
    return res


def yt_check_subscribe(channel_id):
    raise NotImplementedError("yt_check_subscribe not implemented")