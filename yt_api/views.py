import requests
import validators

from django.views import View
from django.views.generic import TemplateView
from django.http import HttpResponse
from django.shortcuts import render

from rest_framework.views import APIView
from rest_framework import generics
from rest_framework import viewsets
from rest_framework.response import Response

from .models import YtChannel, YtChannelStats, YtChannelGroup
from .serializers import YtChannelGroupSerializer, YtChannelSerializer
from .utils import (
    yt_decode_xml_to_msg,
    yt_subscriber,
    yt_get_channel_id,
    yt_get_channel_info,
    yt_ch_polit,
    yt_ch_rust,
    tg_send_message,
    TG_TOKEN,
    TG_MY_CHAT_ID,
)


yt_list = []


class YTChannelGroupViewSet(viewsets.ModelViewSet):
    queryset = YtChannelGroup.objects.all()
    serializer_class = YtChannelGroupSerializer


class YTChannelViewSet(viewsets.ModelViewSet):
    queryset = YtChannel.objects.all()
    serializer_class = YtChannelSerializer


class YTCallback(View):
    def get(self, request):
        hub_challenge = request.GET.get("hub.challenge", )
        if hub_challenge:
            print("GET*", request.GET.get("hub.topic", ))
            return HttpResponse(hub_challenge, status=200)
        else:
            print("GET* EMPTY")
            return HttpResponse("Use it for pubsub hub.challenge", status=204)

    def post(self, request):
        my_xml = request.body.decode('utf-8')
        msg = yt_decode_xml_to_msg(my_xml)
        if msg:
            yt_id = msg["video_id"]
            if yt_id not in yt_list:
                yt_list.append(yt_id)
                tg_send_message(TG_TOKEN, TG_MY_CHAT_ID, msg, (True, True))
        return HttpResponse("Success", status=201)


def sub_test(request):
    res = ""
    for channel_id in yt_ch_rust.keys():
        res += "<p>" + yt_subscriber(channel_id) + "</p>"
    for channel_id in yt_ch_polit.keys():
        res += "<p>" + yt_subscriber(channel_id) + "</p>"
    return HttpResponse("<html><body>"+res+"</body></html>", status=222)


def yt_sub_by_id(request, channel_id):
    res = "<p>" + yt_subscriber(channel_id) + "<b> OK </b></p>"
    return HttpResponse("<html><body>" + res + "</body></html>", status=222)


class AboutView(TemplateView):
    template_name = "checkout/index.html"

    def get(self, request, **kwargs):
        print("get", request.GET)
        context = {}
        return render(request, self.template_name, context=context)

    def post(self, request):
        context = {}
        if "yt_link" in request.POST and validators.url(request.POST["yt_link"]):
            cid = yt_get_channel_id(request.POST["yt_link"])
            if cid:
                context["channel_id"] = cid
                context["ch"] = yt_get_channel_info(cid)
                context["show_channel_form"] = True
                context["yt_link"] = request.POST["yt_link"]

                try:
                    my_ch = YtChannel.objects.get(cid=cid)
                except YtChannel.DoesNotExist:
                    my_ch = None

                if not my_ch:
                    my_ch = YtChannel(
                        cid=context["ch"]["cid"],
                        title=context["ch"]["title"],
                        description=context["ch"]["description"],
                        registration_date=context["ch"]["publishedAt"],
                        thumbnails_url=context["ch"]["thumbnails"],
                    )
                    my_ch.save()
                    print(my_ch.id, my_ch)
                else:
                    context["ch_db"] = "already in DB"
        return render(request, self.template_name, context=context)
