from django.contrib import admin

from .models import YtChannel, YtChannelStats, YtChannelGroup

admin.site.register(YtChannel)
admin.site.register(YtChannelStats)
admin.site.register(YtChannelGroup)
