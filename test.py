def chain_sum(num=None, my_arr=[]):
    if isinstance(num, int):
        my_arr.append(num)
    elif not num:
        return (sum(my_arr), my_arr.clear())[0]
    return chain_sum


print(chain_sum())
print(chain_sum(5)())
print(chain_sum(5)("2")(7)())
print(chain_sum(5)(100)(-10)())
