from django.contrib import admin
from django.urls import path, include
from women.views import WomenViewSet
from rest_framework import routers
from yt_api.views import (
    YTChannelGroupViewSet,
    YTChannelViewSet,
    YTCallback,
    sub_test,
    yt_sub_by_id,
    AboutView,
)


router = routers.SimpleRouter()
router.register(r"ytgroup", YTChannelGroupViewSet)
router.register(r"ytchannel", YTChannelViewSet)


urlpatterns = [
    path('', AboutView.as_view()),
    path('admin/', admin.site.urls),
    path("api/v1/", include(router.urls)),

    path('callback/', YTCallback.as_view()),

    path('sub_test', sub_test),
    path('subid/<str:channel_id>', yt_sub_by_id),
]
